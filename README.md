# I. Introduction to Test-Driven development (TDD)
## 1. Definition
Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle: Requirements are turned into very specific test cases, then the software is improved to pass the new tests, only. This is opposed to software development that allows software to be added that is not proven to meet requirements.
Test-driven development is related to the "test-first" programming concepts of extreme programming.

## 2. Benefits
- Writing tests first require you to really consider what you want from the code
- Short feedback loop
- Creates a detailed specification
- Reduced time in rework
- Less time spent in the debugger and when it is required you usually get closer to problem quickly
- Tells you whether your last change (or refactoring) has broken previously working code
- Allows the design to evolve and adapt to your changing understanding of the problem.

- Simplification
  - Forces radical simplification of the code – you will only write code in response to the requirements of the tests.
  - Forces you to write small classes focused on one thing.
  - Helps create loosely coupled code
- The resulting Unit Tests are simple and act as documentation for the code
- Improves quality and reduces bugs by:
  - forcing us to consider purpose (and the specification of code)
  - simplifying code (many bugs come from the complexity)
  - ensuring changes and new code don’t break the expectations of existing code
  
## 3. Disadvantages
- Hard to learn
- Reduce productivity

# II. Getting Started

## 1. Using unit testing frameworks
- `JUnit` for Java
- `NUnit` for .NET
- `unittest` (used to called `PyUnit`) for Python
- `CppUnit` for C++
- `SUnit` for Smalltalk (base unit testing framework for all `XUnit` testing framework)
  
## 2. Assertions
An assertion is a boolean expression at a specific point in a program which will be true unless there is a bug in the program. A test assertion is defined as an expression, which encapsulates some testable logic specified about a target under test.  

### Benefits of Assertions

The main advantage of having assertions is to identify defects in a program. The usefulness of assertions include:  
- It is used to detect subtle errors which might go unnoticed.
- It is used to detect errors sooner after they occur.
- Make a statement about the effects of the code that is guaranteed to be true.

### Limitations of Assertion

- Like any other piece of code, assertions may themselves contain errors. It can lead to the following problems:  
- Failing to report a bug that exists.
- Reporting an error when it does not exist.
- Can lead to other side effects
- Can Take time to execute if it contains errors and occupies memory as well.

### Examples
Example of assertions in programming language and frameworks:  
Java
``` 
void someMethod()
{
  assert x > 10;
}
```

Objective-C  
``` 
NSAssert(x > 10, @"Error! x not more than 10");
```

JUnit framework
``` 
assertEquals(x, 10)
assertEquals(1, 1);  //pass, do nothing
assertEquals(1, 2);  //fail, error
assertArrayEquals(array1, array2);
assertNotNull(obj1);
assertNull(obj1);
assertSame(obj1, obj2);
assertNotSame(obj1, obj2);
assertTrue(condition);
assertFalse(condition);
```

## 3. Create a test
An example of a unit test in Java
``` 
import static org.junit.Assert.*;

public class BankAccount {
  public int balance;
  
  public void deposit(int i) {
    balance += i;
  }
}

public class BankAccountTest {
  @Test
  public void test() {
    BankAccount acc = new BankAccount();
    acc.deposit(50);
    assertEquals(acc.balance, 50);
  }
}
```

# III. Working with Tests

## 1. The process of TDD
### `Red, Green, Refactor` principle:  
- Red: make the test fail
- Green: make that task pass
- Refactor: make it right

### Process of TDD:  
1. Before you write any new code, write a failing test.
2. Watch the test fail
3. Write application logic - as simple as possible
4. Pass the test
5. Refactor, removing duplication
6. Pass the test again

## 2. Adding tests and removing duplication

On previous BankAccount example, now we have add `withdraw` method and change `balance` to private

``` 
import static org.junit.Assert.*;

public class BankAccount {
  private int balance;
  
  public getBalance() {
    return balance;
  }
  
  public void deposit(int i) {
    balance += i;
  }
  
  public void withdraw(int i) {
    balance -= i;
  }
}

public class BankAccountTest {
  @Test
  public void test() {
    BankAccount acc = new BankAccount();
    acc.deposit(50);
    assertEquals(acc.getBalance(), 50);
    
    acc.withdraw(30);
    assertEquals(acc.getBalance(), 20);
  }
}
```

## 3. Create multiple test methods

Split tests for deposit and withdraw methods

``` 
import static org.junit.Assert.*;
public class BankAccount {
  private int balance;
  
  public BankAccount(int i) {
    balance = i;
  }
  
  public BankAccount() {
    balance = 0;
  }
  
  public getBalance() {
    return balance;
  }
  
  public void deposit(int i) {
    balance += i;
  }
  
  public void withdraw(int i) {
    balance -= i;
    if (balance < 0) {
      balance -= 5;
    }
  }
}

public class BankAccountTest {
  @Test
  public void testDeposit() {
    BankAccount acc = new BankAccount();
    acc.deposit(50);
    assertEquals(acc.getBalance(), 50);
  }
  
  @Test
  public void testWithdraw() {
    BankAccount acc = new BankAccount(75);
    acc.withdraw(50);
    assertEquals(acc.getBalance(), 25);
  }
  
  @Test
  public void testWithdrawWithPenalty() {
    BankAccount acc = new BankAccount(10);
    acc.withdraw(20);
    assertEquals(acc.getBalance(), -15);
  }
}
```

Each test method should be self-contain, independent to each others. That means which test runs first or later it is not important.

## 4. Naming unit tests and test methods
Use name of unit being tested, and add specifics
For example:
```  
testWithdraw() {}  // some languages require 'test'
withdraw() {}
withdrawWithPenalty() {}
```

# IV. Independent techniques

## 1. Testing return values
Testing a void method's side effect
``` 
@Test
public void testWithdraw() {
  BankAccount acc = new BankAccount(100);
  acc.withdraw(50);
  assertEquals(25);
}
```

Directly testing a return value
``` 
@Test
public void testWithdraw() {
  BankAccount acc = new BankAccount(100);
  assertEquals(acc.withdraw(75), 25);
}
```

Testing multiple results
``` 
@Test
public void testWithdraw() {
  BankAccount acc = new BankAccount(100);
  assertFalse(acc.withdraw(1000));  // should return false
  assertEquals(acc.getBalance(), 100);  // verify no change
  
  assertTrue(acc.withdraw(1));  // should return true
  assertEquals(acc.getBalance(), 99);  //veryfiy change
}
```

## 2. Creating a test for expected exceptions
Testing expected exceptions  
JUnit
``` 
@Test (expected=IllegalArgumentException.class)
public void testDivisionException () {
  Calculator c = new Calculator();
  int a = 5, b = 0;
  c.divide(a, b);  // should throw exception
}
```

In Python
``` 
class CalculatorTestCase(unittest.TestCase)
  
  def test_divide(self):
  calc = Calculator()
  self.assertRaises(ValueError, calc.divide, 5, 0)
```

## 3. Setting up and tearing down

``` 
@Before
public void setUp() throws Exception {
  // any needed setup
  acc = new BankAccount(100)
}

@After
public void tearDown() throws Exception {
  // release anything you need
}

//or before and after a specific class
@BeforeClass
public void setUpBeforeClass() throws Exception {
  // run once, before any tests
}

@AfterClass
public void tearDownAfterClass() throws Exception {
  // run once, after all tests
}

@Test
public void testDeposit() {
  acc.deposit(50);
  assertEquals(acc.getBalance(), 50);
}

@Test
public void testWithdraw() {
  acc.withdraw(50);
  assertEquals(acc.getBalance(), 50);
}

@Test
public void testWithdrawWithPenalty() {
  acc.withdraw(110);
  assertEquals(acc.getBalance(), -15);
}
```

# V. Additional Topics

## 1. Mock objects
A Mock Object is a simulated object that mimics the behavior of a real object in controlled ways. Mock objects are often employed in unit testing to scrutinize the performance of actual objects. In this context, an object is the smallest testable part of an application. A mock object makes use of the same interface as the element of code it is intended to imitate.
### Typical reasons for Mock Objects:
- Real object hasn't been write yet
- What you're calling has a UI / needs human interaction
- Slow or difficult to set up
- External resources: file system, database, network, printer
- Non-deterministic behavior
For example:
``` 
@Test
public void testWeatherLookup {
  // instantiate mock object
  WeatherService ws = new MockWeatherService();
  // optional: mock object can be told to expect a certain value
  ws.expectZip(85253);
  
  // use the mock object for repeatable results
  String[] forecast = ws.getForecast(myZip);
  // act on the results
  // assert some behavior
}
```
### Fake Object vs. Mock Objects  
#### Fake Objects:
- Match the original (or intended) method implementations  
- Return pre-arranged results  
Mock Objects also verify interaction
- Assert the expected values from unit under test

#### Mock Objects Frameworks
- Provide structure for defining mock objects
- Can aid in generating methods stubs
- Often provide prearranged mock objects (file stream, console, network, printer equivalents)
- For example: `jMock`, `easymock`, `mockito` (Java), `Microsoft Fakes`, `moq` (.NET), `OCMock` (Objective-C), `mock` (Python), etc.

## 2. Code coverage
Code coverage is a measure used to describe the degree to which the source code of a program is tested by a particular test suite. A program with high code coverage has been more thoroughly tested and has a lower chance of containing software bugs than a program with low code coverage.
Tools:
- `EMMA`, `Clover` (Java)
- `dotCover`, `NCover` (.NET)
- `coverage` (Python)

## 3. TDD Recommendations
### Numbers - what to expect
- One test case / test fixture for each class
- 3-5 tests methods for each class method

### What to test?
- A test for every branch: `if/else/and/or/case/for/while/polymorphism`
- "Test until fear turns to boredom"
- Use code coverage tools

### What to avoid
#### Unit test should not:
- Interact with a database or file system
- Require non-trivial network communication
- Require environment changes to run
- Call complex collaborator objects

### Adding tests to existing projects
- Approach #1: create a complete suite of unit tests (usually a bad idea)
- Approach #2: create tests as needed (recommended)
